/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alexanderpenafiel;

/**
 *
 * @author Michael Peñafiel
 */
public class Pila {
    private Object[] arreglo;
    private int tope;
    private int elementomax;
    
    public Pila (){
        this.elementomax=100;
    }
    public Pila(int elementomax)
  {
    arreglo=new Object[elementomax];
    tope=-1; // inicialmente la pila esta vacía
  }

    public void apilar(Object x)
  {
    if (tope+1<elementomax) 
    {
      tope++;
      arreglo[tope]=x;
    }
  }

  public Object desapilar()
  {
    if (!estaVacia()) 
    {
      Object x=arreglo[tope];
      tope--;
      return x;
    }
        return null;
  }
  

  public Object tope()
  {
    if (!estaVacia()) 
    {
      Object x=arreglo[tope];
      return x;
    }
        return null;
  }
  

  public boolean estaVacia()
  {
        return tope==-1;
  }

    public Object[] getArreglo() {
        return arreglo;
    }

    public void setArreglo(Object[] arreglo) {
        this.arreglo = arreglo;
    }

    public int getTope() {
        return tope;
    }

    public void setTope(int tope) {
        this.tope = tope;
    }

    public int getElementomax() {
        return elementomax;
    }

    public void setElementomax(int elementomax) {
        this.elementomax = elementomax;
    }


    
}
